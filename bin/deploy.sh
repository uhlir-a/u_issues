#!/bin/bash

set -e

###############################
## DJANGO DEPLOY SCRIPT v0.1 ##
###############################

# CONFIG
PROJECT_NAME="u_issues"
SERVICE_NAME=$PROJECT_NAME
PROJECT_PATH="/var/www/u_issues"
DJANGO_APP="./app"
VENV_HOME="/home/u_issues/venv"

##############################################################################

# Go to root of the project
cd $PROJECT_PATH

# Update the repo
git fetch --tags
latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
git checkout $latestTag # Checkout latest tag

# Activate virtualenv for the project
source "${VENV_HOME}/bin/activate"

# Install dependencies
pip install -r requirements.txt

cd $DJANGO_APP
./manage.py migrate # Apply DB migrations
./manage.py compilemessages # Compile translations

# Build front-end dependencies
cd ..
npm install
./node_modules/.bin/bower install
./node_modules/.bin/gulp build --production

cd $DJANGO_APP
./manage.py collectstatic --no-input

# Restart server
sudo systemctl restart $SERVICE_NAME

curl --header 'Access-Token: XXX' \
     --header 'Content-Type: application/json' \
     --data-binary "{\"body\":\"Deployment of U_Issues web version ${latestTag} was finished.\",\"title\":\"U_Issues  deploy done\",\"type\":\"note\"}" \
     --request POST \
     https://api.pushbullet.com/v2/pushes
