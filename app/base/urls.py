from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
import debug_toolbar
from django.conf.urls.static import static

from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(next_page='home'), name='logout'),

    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    url(r'^issues/', include('issues.urls')),

    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
