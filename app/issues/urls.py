from django.conf.urls import url
from issues import views

urlpatterns = [
    url(r'^$', views.IssueList.as_view(), name='issues_list'),
    url(r'^new/?$', views.IssueCreate.as_view(), name='issues_new'),
    url(r'^(?P<pk>[0-9]+)/update/?$', views.IssueUpdate.as_view(), name='issues_update'),
    url(r'^(?P<pk>[0-9]+)/delete/?$', views.IssueDelete.as_view(), name='issues_delete'),
]