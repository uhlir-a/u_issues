from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Avg, F, Max, Min
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, UpdateView, CreateView
from issues.models import Issue


class SuperUserOnlyMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_superuser


class IssueList(LoginRequiredMixin, ListView):
    model = Issue
    context_object_name = 'issues'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stats'] = {
            "avg": Issue.objects.filter(state="S").aggregate(val=Avg(F('solved_ts') - F('created_ts'))),
            "min": Issue.objects.filter(state="S").aggregate(val=Min(F('solved_ts') - F('created_ts'))),
            "max": Issue.objects.filter(state="S").aggregate(val=Max(F('solved_ts') - F('created_ts'))),
        }

        return context


class IssueCreate(SuperUserOnlyMixin, CreateView):
    model = Issue
    fields = ['description', 'category']
    success_url = reverse_lazy('issues_list')
    template_name_suffix = '_create_form'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class IssueUpdate(SuperUserOnlyMixin, UpdateView):
    model = Issue
    success_url = reverse_lazy('issues_list')
    fields = ['description', 'category', 'state', 'solver']
    template_name_suffix = '_update_form'

    def form_valid(self, form):
        if form.initial['state'] != form.cleaned_data['state']:
            if form.cleaned_data['state'] == 'S':
                form.instance.solved_ts = datetime.now()
            else:
                form.instance.solved_ts = None

        return super().form_valid(form)


class IssueDelete(SuperUserOnlyMixin, DeleteView):
    model = Issue
    success_url = reverse_lazy('issues_list')
