from django.apps import AppConfig


class UIssuesConfig(AppConfig):
    name = 'u_issues'
