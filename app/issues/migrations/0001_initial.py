# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-14 18:32
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', models.CharField(choices=[('N', 'New'), ('A', 'Active'), ('S', 'Solved')], default='N', max_length=1)),
                ('description', models.TextField()),
                ('created_ts', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('solved_ts', models.DateTimeField(blank=True, verbose_name='Solved date')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='issue_author', to=settings.AUTH_USER_MODEL)),
                ('solver', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='issue_solver', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['created_ts'],
            },
        ),
    ]
