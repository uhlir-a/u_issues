# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-14 19:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='solved_ts',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Solved date'),
        ),
    ]
