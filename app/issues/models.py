from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.text import Truncator


class Category(models.Model):
    name = models.CharField(max_length=40)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Issue(models.Model):
    STATES = (
        ('N', 'New'),
        ('A', 'Active'),
        ('S', 'Solved')
    )

    state = models.CharField(max_length=1, default='N', choices=STATES)
    description = models.TextField()
    created_ts = models.DateTimeField('Creation date', auto_now_add=True)
    solved_ts = models.DateTimeField('Solved date', null=True, blank=True)

    author = models.ForeignKey(User, related_name='issue_author')
    category = models.ForeignKey(Category)
    solver = models.ForeignKey(User, blank=True, null=True, related_name='issue_solver')

    class Meta:
        ordering = ["created_ts"]

    def clean(self):
        if self.state == "S" and not self.solver:
            raise ValidationError("When Issue is Solver it needs to have assigned an User which Solved the issue!")

    def __str__(self):
        return Truncator(self.description).chars(30)

    def get_absolute_url(self):
        return
