#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

/node_modules/.bin/gulp &

python manage.py migrate
python manage.py runserver 0.0.0.0:8000
