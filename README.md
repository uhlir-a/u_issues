# U_Issues

## Installation

**Requirements:** Docker (>= 1.10), Docker-Compose

 1. Clone repo
 2. Create conf.env file: `cp conf.env.example conf.env`
 3. Build containers: `docker-compose build`
 4. Run it: `docker-compose up`
 5. Create main super-user: `docker-compose run django ./manage.py createsuperuser` (you can then create other users through Administration)
 6. Enjoy!
 
 The app is currently more in ***development*** state, then production-ready, as it would need more tweaks especially 
 from the configuration side. But I think it was not required to have it fully production-ready and deployed.
 
## Fictional TODOs

Some TODOs which I would continue to work on, if this would be real thing:

 - Tests coverage
 - Production Docker
 - Full extensive Environment-variables based configuration
 - Better frontend dependencies management (drop Bower, rework Gulp)
 